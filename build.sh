# Parameters: DOCKERHUB_USER_ID DOCKERHUB_PASSWORD

if [ "$#" -eq 2 ]; then
	docker login -u $1 -p $2
	docker push ${IMAGE_NAME}
fi

${MVN} ${MVN_TOOLCHAIN_OPT} clean package
mkdir -p scratch
cp main/target/*.jar scratch
docker build -t ${IMAGE_NAME} -f Dockerfile scratch
